from trainerbase.gui import add_codeinjection_to_gui, simple_trainerbase_menu

from injections import god_mode, one_hit_kill


@simple_trainerbase_menu("Lizards Must Die!", 300, 150)
def run_menu():
    add_codeinjection_to_gui(god_mode, "God Mode", "F1")
    add_codeinjection_to_gui(one_hit_kill, "One Hit Kill", "F2")
