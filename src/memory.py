from pymem import process
from trainerbase.memory import pm


game_assembly_base_address = process.module_from_name(pm.process_handle, "GameAssembly.dll").lpBaseOfDll
