from trainerbase.codeinjection import AllocatingCodeInjection, CodeInjection

from memory import game_assembly_base_address


god_mode = CodeInjection(game_assembly_base_address + 0x406D72, "add [rdi + 0x20], ecx")

one_hit_kill = AllocatingCodeInjection(
    game_assembly_base_address + 0x40C2CF,
    """
        pop rax

        xor ecx, ecx
        sub ebx, eax
        cmovns ecx, ebx
        add ecx, 100000
        sub [rdi + 0x20], ecx
        xor edx, edx
        mov [rsp + 0x48],ecx

        push rax
    """,
    original_code_length=16,
    is_long_x64_jump_needed=True,
)
